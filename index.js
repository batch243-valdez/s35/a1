
const express = require("express");

const app = express();

const port = 3001;


const mongoose = require("mongoose");

mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.eiw7ahh.mongodb.net/?retryWrites=true&w=majority", 
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
)

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("You are connected to your Database"));

const userSchema = new mongoose.Schema({
    username: "string",
    password: "string"
});

const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create User
app.post("/signup", (req, res) => {
    
    User.findOne({username:req.body.username}, (err, result) => {

        if(result != null && result.username == req.body.username){
            return res.send("Duplicate Task Found")
        }
        else{
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })
            
            newUser.save((saveErr, savedUser) => {
                if(saveErr){
                    return console.error(saveErr);
                }
                else{
                    return res.status(201).send("New user succesfully registered!")
                }
            })         
        }
        
    })
})

// GET Users
app.get("/getusers", (req, res) => {

    User.find({}, (err, result) =>{

        if(err){
            return console.error(err);
        }
        else{
            return res.status(200).json({
                data:result
            })

        }
    })
})




app.listen(port, () => console.log(`Server runnig at ${port}`));
